
<h1 style="text-align: center"> Coursework 2 </h1>
<hr>


Introduction
=========
 

- We should have a unit-test for each feature of the program. During software development adding a new feature can break something that is seemingly completely unrelated. Its important that our tests cover as much as possible, if not everything so we catch errors before they are committed to the repository.

- Make sure users cannot break the system by doing unexpected, or malicious things. Using the  `big list of naughty strings` repository, we should all places where users can input text to make sure they are safe from text-formatting/processing bugs and vulnerabilities.



Product plan
=========

I think we should implement an mvp for the server first, and then focus on the client side things. 
We can use HTTP to test the server. As the client will be using requests as well, we should be able to test all features of the server without needing a client. We should also focus on server-side string formatting bugs here, using the `big list of naughty strings` repository.

Client side testing can be done in 2 different ways. 
- We can ship extra code to the browser which tests individual functions.
- Secondly using a framework like puppeteer we can test the user interface of the game by simulating a real user.

Using the CI server on CSgitlab we can make sure these tests always run before any code is committed into the shared repository. If set-up correctly we can make the CI server reject any commits that do not meet the testing standard.


