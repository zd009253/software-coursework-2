Quality plan
=================

It is vital to set quality goals for the project as developers need to follow steps in order to stick to the product plan and to make sure it follows the S.M.A.R.T. steps: Specific, Measurable, Achievable, Realistic and Timely.

#### Specific:
We need to set specific goals so it is clear what the task is and easy enough to be understood by the developers, the more specific it is, the more unambiguous it would sound;

#### Measurable:
The developers need to be clear and precise with their documentation in testing so they stick to the plan set for them;

#### Achievable:
The goals must be attainable because we do not want to go beyond our knowledge and skills as developers to think of something completely unrealistic which correlates to our next step;

#### Realistic:
We want our goals for the testing and the project to be realistic so we do not have falsely high expectations of the end product;

#### Timely:
We want to set a time frame for the testing and the project itself so the creators do not get carried away by their tasks.
