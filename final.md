<center><h1 style="color: black"><b> Coursework 2 </b></h1></center> <hr>

* Module Code: CS1SE20
* Assignment report Title: 
* Student Number (e.g. 25098635): 30000573, 30009253, 30021318, 30005130, 30018413, 30001050
* Date (when the work completed): 11/03/2022
* Actual hrs spent for the assignment: 20
* Assignment evaluation (3 key points): 


[gitlab repository](https://csgitlab.reading.ac.uk/zd009253/software-coursework-2)
<br><br> <center><h2>Introduction (1)</h2></center>

Testing is an essential part of software development as it offers an automated way of making sure a programme does what it is expected to do. In a large project like ours, there are many important things to test.

In this project we aim to focus on 2 main types of testing. Firstly, we want to write unit-tests for the features we have already implemented. This is vital as when new features are added or bugs get fixed, they often affect the functioning of the programme in unexpected ways. The tests warn us when such things happen.

Secondly, we want to run tests for a security and stability standpoint. Any part of a programme that has user input, will inevitably get unexpected or even malicious text inputted into it. We want to test each input field to make sure our filtering works.


 <br><br> <center><h2>Product plan (2)</h2></center>
![[Images/Flowchart_Product_Plan.png]]

This flowchart consists of 5 steps that ensure the process goes smoothly with minimal obstacles.

#### Process 1:
Creating the minimum viable product for the server. This involves creating all the essential functions the server needs, but ignoring less important ones that can be implemented later.

#### Process 2:
Writing separate unit-tests for each feature of the server.

#### Process 3:
Writing the minimum viable product for the client. This part can ignore most of the ease-of-use and design features in favour of getting the functionality out.

#### Process 4:
Writing separate unit-tests for each feature of the client.

#### Process 5:
Once most of the features are done, we can focus on writing tests that test the stability and security of the server. For example we can send un-expected or badly formatted requests to the server to test whether it crashes.



After these processes we will have an MVP ready. We can then start working on cleaning up, and polishing the product.


<br><br> <center> <h2> Quality goals (3)</h2></center> 
It is vital to set quality goals for the project as developers need to follow steps in order to stick to the product plan and to make sure it follows the S.M.A.R.T. steps: Specific, Measurable, Achievable, Realistic and Timely. 

#### Specific:
We need to set specific goals so it is clear what the task is and easy enough to be understood by the developers, the more specific it is, the more unambiguous it becomes;

#### Measurable:
The developers need to be clear and precise with their documentation in testing so they stick to the plan set for them;

#### Achievable:
The goals must be attainable because we do not want to go beyond our knowledge and skills as developers to think of something completely unrealistic, which correlates to our next step;

#### Realistic:
We want our goals for the testing and the project to be realistic so we do not have falsely high expectations of the end product;

#### Timely:
We want to set a time frame for the testing and the project itself so we as creators do not get carried away by the tasks given.

Our main goals are:
All features should have component tests - so we know they work and keep working the same way;
Carry tests to ensure the code is stable enough in case of unexpected input.



<br><br> <center> <h2> Test Process and Plans (4) </h2></center> 

### Test Process (4.1)
Testing can be carried out iteratively to test smaller functions of the code. This allows for the smaller errors to be fixed and then have the process repeated to the next section. This also allows a developer to go through the whole code and test everything and change to fix/improve it.

Testing can also be done terminally, which is a test on the whole project to see if it runs smoothly. This can be used to check that no unintended errors have been made with the project when doing iterative testing.

Source: [https://learnlearn.uk/alevelcs/methods-of-testing/](https://learnlearn.uk/alevelcs/methods-of-testing/ "https://learnlearn.uk/alevelcs/methods-of-testing/") 



### Component testing (4.2)
Component testing is done after the first phase of testing has been completed. During this phase of testing, it is best to test each component individually without integrating the other components. The main reason component is important is because it validates the behavior of each individual component. 
Source: https://economictimes.indiatimes.com/definition/component-testing


### System testing (4.3)
System testing is defined as a testing method, which allows you to carry out a full test to the full integrated system. System Testing is the level of software testing performed before Acceptance Testing and after Integration Testing.


### Acceptance testing (4.4)
Acceptance testing is a test to check accessibility for the end users

 Acceptance testing is a test to check how people will use the code and to check if it is accessible to the users. In acceptance testing customers test a system to decide whether it's ready to be accepted from the actual system developers. You have a direct order when carrying out this test. It goes along the lines of testing the criteria first, then testing the plan, and then testing the results and creating a final report.

Source: [https://www.geeksforgeeks.org/acceptance-testing-software-testing/](https://www.geeksforgeeks.org/acceptance-testing-software-testing/#:~:text=Acceptance%20Testing%20is%20a%20method,acceptable%20for%20delivery%20or%20not).





<br><br> <center> <h2> Test Cases (5) </h2></center> 

### Tests for a valid/invalid login attempts (5.1)
```Gherkin
Feature: Login

  Background: 
    Given a username and password are entered into a login prompt

  Scenario: Valid Attempt
    When user tries to login with valid credentials
    Then attemptLogin(username, hashedpassword) will be called with those credentials
    And they will match those returned from the database through that
    * user is logged in

  Scenario: Invalid Attempt
    When user tries to login with invalid credentials
    Then attemptLogin(username, hashedpassword) will be called with those credentials
    And they will not match those returned from the database through that
    * an error message will be shown
    * the user will not be logged in

```
                                
### Tests for adding a new high score entry (5.2)
```gherkin
Feature: Adding new high score entry

  Background: 
    Given a game has ended
    Given previous high scores are accessed from the database using getHighscores()
 
  Scenario: New top score has been reached
    When the current score is compared to the current top score
    And the current score is higher than the current top score
    Then write that score into the database at the top
    And the associated username using addHighscore(username,score)

  Scenario: New high score to be added to the leaderboard, but not necessarily a new top score
    When the current score is compared one by one to the current high scores in the database
    And a high score is reached in the list which is less than the current score
    Then write the current score into the database above that one
    And the associated username using addHighscore(username,score)
    * in the right place in the list of high scores in the database 
```
    
### Tests for sanitising user entered text  (5.3)

```gherkin
Feature: Sanitising user entered text
  Background:
    Given user has entered some text
    
    Scenario: ASCII punctuation is entered as input
      When say invalid punctuation in this scenario such as brackets are entered
      Then reject the input text
      And display an error message to the user

  Scenario: Only letters are wanted
    When text containing a number mixed in with the letters is entered
    Then take out the number
    And accept the newly modified input
      
  Scenario: Script injection is attempted
    When input matches one of the scripts from the big-list-of-naughty-strings
    Then reject the input to prevent the injection
    And display an error message to the user          
```



<br><br> <center> <h2> Conclusion </h2></center> 

Testing is an important part in software development. In our project we make sure every step is carefully defined, considered and tackled. We write a product plan, quality goals, test process and plans, and test cases. 
