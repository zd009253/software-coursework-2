
Product plan
==================
![[Flowchart_Product_Plan.png]]


#### Process 1:
Creating the minimum viable product for the server. This involves creating all the essential functions the server needs, but ignoring less important ones that can be implemented later.

#### Process 2:
Writing separate unit-tests for each feature of the server.

#### Process 3:
Writing the minimum viable product for the client. This part can ignore most of the ease-of-use and design features in favour of getting the functionality out.

#### Process 4:
Writing separate unit-tests for each feature of the client.

#### Process 5:
Once most of the features are done, we can focus on writing tests that test the stability and security of the server. For example we can send un-expected or badly formatted requests to the server to test whether it crashes.



After these processes we will have an MVP ready. We can then start working on cleaning up, and polishing the product.