<h1 style="text-align: center"> Coursework 2 </h1>
<hr>


Introduction
=========

Testing is an essential part of software development as it offers an automated way of making sure a programme does what it is expected to do. In a large project like ours, there are many important things to test.
In this project we aim to focus on 2 main types of testing. Firstly we want to write unit-tests for the features we have already implemented. This is important as when new features are added or bugs get fixed, they often affect the functioning of the programme in unexpected ways. The tests warn us when such things happen.
Secondly we want to run tests for a security and stability standpoint. Any part of a programme that has user input, will inevitably get unexpected or even malicious text inputted into it. We want to test each input field to make sure our filtering works.


